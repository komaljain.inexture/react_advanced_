//Dependencies:
//yarn add express cors twilio

const express = require("express");
const cors = require("cors");
const twilio = require("twilio");

//twilio requirements -- Texting API
const accountSid = "AC92c5cb36c7c86493c3f4edf6c7e8db31";
const authToken = "e5c42a149babb835f6ac9667f6e9d19b";
const client = new twilio(accountSid, authToken);

const app = express(); //alias

app.use(cors()); //Blocks browser from restricting any data

//Welcome Page for the Server
app.get("/", (req, res) => {
  res.send("Welcome to the Express Server");
});

//Twilio
app.get("/send-text", (req, res) => {
  //Welcome Message
  res.send("Hello to the Twilio Server");

  //_GET Variables
  const { recipient, textmessage } = req.query;

  //Send Text
  client.messages
    .create({
      body: textmessage,
      to: "+917597907940", // Text this number
      from: "+12182204147", // From a valid Twilio number
    })
    .then((message) => console.log(message.body));
});

app.listen(4000, () => console.log("Running on Port 4000"));
