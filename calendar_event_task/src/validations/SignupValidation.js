import * as yup from "yup";

export const schema = yup.object().shape({
  name: yup.string().required("Fullname is required").min(5),
  email: yup.string().required("Please enter valid Email").email(),
  dob: yup.string().required("Please enter Date of birth"),
  password: yup
    .string()
    .required("Password must contain 5 to 15 character")
    .min(5)
    .max(15),
});
