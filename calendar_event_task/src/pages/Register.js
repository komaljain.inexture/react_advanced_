import React, { useState } from "react";
import PhoneInput, { isValidPhoneNumber } from "react-phone-number-input";
import "react-phone-number-input/style.css";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Link } from "react-router-dom";
import { schema } from "../validations/SignupValidation";
import { connect, useDispatch } from "react-redux";
import { CallUserRegister } from "../reducer/action";

const Signup = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    control,
  } = useForm({
    resolver: yupResolver(schema),
    reValidateMode: "onChange",
    mode: "all",
  });

  const dispatch = useDispatch();
  const [value, setValue] = useState();
  //Data storage to localstore
  const onSubmit = (data) => {
    dispatch(CallUserRegister(data));
  };

  console.log("all fields ========>", watch());

  return (
    <div className="outer">
      <form className="box" onSubmit={handleSubmit(onSubmit)}>
        <h3>Sign-Up</h3>
        <div className="form-group">
          <label>Full name</label>
          <input
            type="text"
            id="Fullname"
            className="form-control"
            placeholder="Full name"
            name="name"
            {...register("name")}
          />
          <p>{errors?.name?.message}</p>
        </div>

        <div className="form-group">
          <label>Email</label>
          <input
            {...register("email")}
            type="email"
            className="form-control"
            placeholder="Enter email"
            name="email"
          />
          <p>{errors?.email?.message}</p>
        </div>
        <div className="form-group">
          <label>Phone Number</label>
          <Controller
            name="phone-input"
            control={control}
            rules={{
              validate: (value) => isValidPhoneNumber(value),
            }}
            render={({ field: { onChange, value } }) => (
              <PhoneInput
                value={value}
                onChange={onChange}
                defaultCountry="US"
                id="phone-input"
              />
            )}
          />
          <p>{errors?.phone?.message}</p>
          {/* {errors.phone && <p className="error-message">Invalid Phone</p>} */}
        </div>
        <br />
        <div className="form-group">
          <label>Date of Birth</label>
          <input
            {...register("dob")}
            type="text"
            className="form-control"
            placeholder="DD/MM/YYYY"
            name="dob"
          />
          <p>{errors?.dob?.message}</p>
        </div>

        <div className="form-group">
          <label>Password</label>
          <input
            {...register("password")}
            type="password"
            className="form-control"
            placeholder="Enter password"
            name="password"
          />
          <p>{errors?.password?.message}</p>
        </div>

        <button type="submit" className="btn btn-dark btn-lg btn-block">
          Register
        </button>

        <p className="forgot-password text-right">
          Already registered <Link to="/sign-in">log in?</Link>
        </p>
      </form>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

const mapDispatchToProps = {
  CallUserRegister,
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
