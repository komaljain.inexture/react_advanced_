import React, { lazy, Suspense } from "react";
//import { useHistory} from 'react-router-dom';
import { connect, useDispatch } from "react-redux";
import DemoApp from "../components/schedule";
// import Schedule from "../components/schedule";
import { UserLogout, CallUserLogin } from "../reducer/action";

// Get item from localstorage
const Dashboard = () => {
  const dispatch = useDispatch();
  //  const history =useHistory();
  let information = JSON.parse(localStorage.getItem("komal"));
  const user = information[0]["name"];
  console.log("abhsd", information);

  // Remove data after logout
  function remove() {
    dispatch(UserLogout());
  }
  // Dashboard Nav
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <h3 className="navbar-brand">Dashboard</h3>
          <h5>
            Welcome
            <button type="submit" className="btn_logout" onClick={remove}>
              Logout
            </button>
          </h5>
        </div>
      </nav>
      <DemoApp />
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

const mapDispatchToProps = {
  UserLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
