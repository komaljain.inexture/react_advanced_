import React from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { Router as Router, Switch, Route, Link } from "react-router-dom";
import Login from "./pages/Login";
import SignUp from "./pages/Register";
import Dashboard from "./pages/Dashboard";
// import ProtectedRoute from './components/ProtectedRoutes';
import history from "./reducer/history";
// import DetailPage from './screens/Detail-Page';

function App() {
  return (
    <>
      {/* --------------Nav bar--------------------- */}
      <Router history={history}>
        <div className="App">
          <nav className="navbar navbar-light ">
            <div className="container">
              <Link className="navbar-brand" to={"/sign-up"}>
                React App
              </Link>
              <div
                className="collapse navbar-collapse"
                id="navbarTogglerDemo02"
              >
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link className="nav-link" to={"/sign-up"}>
                      Sign up
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to={"/sign-in"}>
                      Login
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
        <Switch>
          <Route exact path="/sign-up" component={SignUp} />
          <Route path="/sign-in" component={Login} />
          <Route path="/Dashboard" component={Dashboard} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
