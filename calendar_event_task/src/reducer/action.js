import { LOGOUT, USER_REGISTER, DASHBOARD } from "./action.type";
import history from "./history";

export function setUserRegister(user) {
  console.log("setUserRegister");
  return {
    type: USER_REGISTER,
    user,
  };
}

export function CallUserRegister(data) {
  return (dispatch) => {
    console.log("CallUserRegister inside", data);
    dispatch(setUserRegister(data));
    let updatedData = [];
    const newData = localStorage.getItem("komal");
    if (newData) {
      updatedData = JSON.parse(newData);
    }
    updatedData.push(data);
    console.log("updatedData", updatedData);
    localStorage.setItem("komal", JSON.stringify(updatedData));

    alert("Registered successfully");
    history.push("/sign-in");
  };
}

export function CallUserLogin(data) {
  // let details = JSON.parse(localStorage.getItem("komal"));
  // // const newData = localStorage.getItem("komal");

  // if (
  //   updatedData["email"]  && updatedData["password"]
  // ) {
  //   history.push("/Dashboard");
  //   console.log("home");
  // } else {
  //   history.push("/sign-in");
  //   console.log("fail");
  //   alert("Invalid email or password");
  // }
  // return {
  //   type: DASHBOARD,
  // };

  if (localStorage.getItem("komal")) {
    const allStoredUsers = JSON.parse(localStorage.getItem("komal"));
    const matchedUser = allStoredUsers.filter((user) => {
      return data.email === user.email && data.password === user.password;
    });
    if (matchedUser.length) {
      history.push("/Dashboard");
      console.log("Login successful");
    } else {
      history.push("/sign-in");
      console.log("fail");
      alert("Invalid email or password");
    }
  }

  return {
    type: DASHBOARD,
  };
}

export function UserLogout() {
  console.log("Logout");
  // localStorage.clear();
  history.goBack("/");
  return {
    type: LOGOUT,
  };
}
