import { USER_REGISTER, USER_LOGIN, LOGOUT, DASHBOARD } from "./action.type";

const initialState = {
  newUser: [],
  isUserRegister: false,
  isUserLogin: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_REGISTER:
      return {
        ...state,
        newUser: action?.user,
      };
    case USER_LOGIN:
      return {
        ...state,
        newUser: {},
      };
    case LOGOUT:
      return {
        newUser: {},
        ...initialState,
      };
    case DASHBOARD:
      return {
        ...state,
      };
    default:
      return state;
  }
};
