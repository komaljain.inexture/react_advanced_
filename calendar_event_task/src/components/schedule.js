import React from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
// import { resourceTimeGridFourDay } from "@fullcalendar/resource-timegrid";
// import resourceDayGridPlugin from "@fullcalendar/resource-daygrid";
// import resourceTimelinePlugin from "@fullcalendar/resource-timeline";
import { INITIAL_EVENTS, createEventId } from "./event-utils";

export default class DemoApp extends React.Component {
  state = {
    weekendsVisible: true,
    currentEvents: [],
  };
  calendarRef = React.createRef();

  render() {
    return (
      <div className="demo-app">
        {/* {this.renderSidebar()} */}
        <div className="demo-app-main">
          {
            <FullCalendar
              // schedulerLicenseKey="XXX"
              plugins={[
                dayGridPlugin,
                timeGridPlugin,
                interactionPlugin,
                // resourceTimelinePlugin,
              ]}
              headerToolbar={{
                left: "prev,next today",
                center: "title",
                right: "dayGridMonth,timeGridWeek,timeGridDay",
              }}
              initialView="dayGridMonth"
              // views={{
              //   timeGridPlugin: {
              //     type: "resourceTimeline",
              //     duration: { weeks: 1 },
              //     // slotDuration: { days: 1 },
              //   },
              // }}
              // resources={[
              //   { id: "a", title: "Room A" },
              //   { id: "b", title: "Room B" },
              // ]}
              selectable={true}
              selectMirror={true}
              dayMaxEvents={true}
              weekends={this.state.weekendsVisible}
              // initialEvents={INITIAL_EVENTS} // alternatively, use the `events` setting to fetch from a feed
              select={this.handleTimeSelect}
              eventContent={renderEventContent} // custom render function
              // eventClick={this.handleEventClick}
              nowIndicator
              ref={this.calendarRef}
              eventsSet={this.handleEvents} // called after events are initialized/added/changed/removed
            />
          }
        </div>
      </div>
    );
  }

  // renderSidebar() {
  //   return (
  //     <div className="demo-app-sidebar">

  //     </div>
  //   );
  // }

  handleWeekendsToggle = () => {
    this.setState({
      weekendsVisible: !this.state.weekendsVisible,
    });
  };

  handleTimeSelect = (selectInfo) => {
    // console.log(selectInfo);
    if (selectInfo.view.type === "timeGridDay") {
      let title = prompt("Please enter a new title for your event");
      let calendarApi = selectInfo.view.calendar;

      // calendarApi.unselect(); // clear date selection

      if (title) {
        calendarApi.addEvent({
          id: createEventId(),
          title,
          start: selectInfo.startStr,
          end: selectInfo.endStr,
          allDay: selectInfo.allDay,
        });
      }
    }
    localStorage.setItem("events", JSON.stringify(this.state.currentEvents));
  };

  // handleEventClick = (clickInfo) => {
  //   if (
  //     window.confirm(
  //       `Are you sure you want to delete the event '${clickInfo.event.title}'`
  //     )
  //   ) {
  //     clickInfo.event.remove();
  //   }
  // };

  handleEvents = (events) => {
    this.setState({
      currentEvents: events,
    });
  };
}

function renderEventContent(eventInfo) {
  return (
    <>
      <b>{eventInfo.timeText}</b>
      <i>{eventInfo.event.title}</i>
    </>
  );
}
