import React from "react";
import database from "../firebase";
import "../App.css";

export default function Todo({ todo }) {
  const deleteTodo = () => {
    const todoRef = database.database().ref("Todo").child(todo.id);
    todoRef.remove();
  };
  const completeTodo = () => {
    const todoRef = database.database().ref("Todo").child(todo.id);
    todoRef.update({
      complete: !todo.complete,
    });
  };
  return (
    <div>
      <h5 className={todo.complete ? "complete" : ""}>{todo.title}</h5>
      <button onClick={deleteTodo}>Delete</button>
      <button onClick={completeTodo}>Complete</button>
    </div>
  );
}
