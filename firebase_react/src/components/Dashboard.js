import React, { useEffect, useState } from "react";
import { Card, Button, Alert, Nav } from "react-bootstrap";
import { useAuth } from "../context/AuthContext";
import { Link, useHistory } from "react-router-dom";
import TodoForm from "./TodoForm";
import database from "../firebase";
import TodoList from "./TodoList";
import Todo from "./Todo";
import Connector from "./connector";

export default function Dashboard() {
  const [error, setError] = useState("");
  const { currentUser, logout } = useAuth();
  const history = useHistory();

  async function handleLogout() {
    setError("");

    try {
      await logout();
      history.push("/login");
    } catch {
      setError("Failed to log out");
    }
  }

  return (
    <>
      {/* <Card>
        <Card.Body>
          <h2 className="text-center mb-4">Profile</h2>
          {error && <Alert variant="danger">{error}</Alert>}
          <strong>Email:</strong> {currentUser.email}
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        <Button variant="link" onClick={handleLogout}>
          Log Out
        </Button>
      </div> */}
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <h3 className="navbar-brand">Profile</h3>
          <h5>
            Welcome {currentUser.email}
            <Button variant="link" onClick={handleLogout}>
              Log Out
            </Button>
          </h5>
        </div>
      </nav>
      {/* <div>
        <TodoForm />
      </div>
      <div>
        <TodoList />
      </div> */}
      <div>
        <Connector />
      </div>
    </>
  );
}
