import React, { useState } from "react";
import CheckboxTree from "react-checkbox-tree";
import "react-checkbox-tree/lib/react-checkbox-tree.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faMinus } from "@fortawesome/free-solid-svg-icons";

export const nodes = [
  {
    id: "0",
    value: "Groceries",
    label: "Groceries",
    children: [
      { id: "1", value: "Almond Meal flour", label: "Almond Meal flour" },
      { id: "2", value: "Organic eggs", label: "organic eggs" },
      { id: "3", value: "Protein Powder", label: "Protein powder" },
      {
        id: "4",
        value: "Fruits",
        label: "Fruits",
        children: [
          {
            id: "41",
            value: "Apple",
            label: "Apple",
          },
          {
            id: "42",
            value: "Berries",
            label: "Berries",
            children: [
              {
                id: "421",
                value: "Raspberry",
                label: "Raspberry",
              },
              {
                id: "422",
                value: "Blueberry",
                label: "Blueberry",
              },
              {
                id: "423",
                value: "berry",
                label: "berry",
              },
            ],
          },
          {
            id: "5",
            value: "Orange",
            label: "Orange",
          },
          {
            id: "10",
            value: "Berry",
            label: "berry",
          },
        ],
      },
    ],
  },
  {
    id: "6",
    value: "Reminders",
    label: "Reminders",
    children: [
      { id: "61", value: "Cook dinner", label: "Cook dinner" },
      {
        id: "62",
        value: "Read the material design spec ",
        label: "Read the material design spec",
      },
      {
        id: "63",
        value: "Upgrade application to React",
        label: "Upgrade application to React",
      },
      {
        id: "64",
        value: "ProteinPowder",
        label: "Protein Powder",
      },
    ],
  },
];
function getFullPath(arr, target) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].value === target) {
      return arr[i].label;
    }
    if (!arr[i].children) {
      continue;
    }
    var path = getFullPath(arr[i].children, target);
    if (path) {
      console.log(arr[i].label + "/" + path);
      return arr[i].label + "/" + path;
    }
  }
}
const Tree = () => {
  const [check, setCheck] = useState();
  const [expand, setExpand] = useState();
  return (
    <div>
      <CheckboxTree
        nodes={nodes}
        checked={check}
        expanded={expand}
        onCheck={(checked) => setCheck(checked)}
        onExpand={(expanded) => setExpand(expanded)}
        icons={{
          check: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-check"
              icon={faCheck}
            />
          ),
          uncheck: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-uncheck"
              icon={["far", "square"]}
            />
          ),
          halfCheck: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-half-check"
              icon={faMinus}
            />
          ),
          expandClose: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-expand-close"
              icon="chevron-right"
            />
          ),
          expandOpen: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-expand-open"
              icon="chevron-down"
            />
          ),
          expandAll: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-expand-all"
              icon="plus-square"
            />
          ),
          collapseAll: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-collapse-all"
              icon={faMinus}
            />
          ),
          parentClose: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-parent-close"
              icon="folder"
            />
          ),
          parentOpen: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-parent-open"
              icon="folder-open"
            />
          ),
          leaf: (
            <FontAwesomeIcon
              className="rct-icon rct-icon-leaf-close"
              icon="file"
            />
          ),
        }}
      />
      <br />
      {check ? check.map((e) => <p>{getFullPath(nodes, e)}</p>) : null}
    </div>
  );
};
export default Tree;
