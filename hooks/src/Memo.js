import React, { useState } from 'react';

export default function Memo(){
    const [number,setNumber]= useState(0);
    const[dark, setDark]= useState(false);
    const doubleNumber = slowFunction(number)
   
    const themeStyles = {
        backgroundColor : dark ? '#000' : '#CCC',
        color : dark ? '#CCC': '#000'
    }

    return(
        <>
        <input  type="number" value={number} onChange={e => setNumber(parseInt(e.target.value))}/>
        <button onClick={()=> setDark(prevDark => !prevDark)}>Change Theme</button>
        <div styles={themeStyles}>{doubleNumber}</div>
        </>
    )
}
function slowFunction(num)
{
    console.log('Calling slow function')
    for(let i=0; i<=1000000000;i++){}
    return num *2
}