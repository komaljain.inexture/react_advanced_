import React, { useState } from 'react'
import Board from './Board'
import Message from './Message'
import Refresh from './Refresh'



const isWon = (board) => {

    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        let [a, b, c] = lines[i];
       
        if (board[a] !== "" && board[a] === board[b] && board[a] === board[c]) {
            return true;

        }

    }
    return false;
}


const Game = () => {
   
    const [board, setBoard] = useState(Array(9).fill(""));
    const [isPlayer, setIsPlayer] = useState("X");
    const [message, setMessage] = useState("Click to Start");



    const refresh = () => {
        setBoard(Array(9).fill(""));
        setMessage("Click to start");
        setIsPlayer("X");

    }


    const handleInput = (position) => {
        if (isPlayer === "" || board[position] !== "") {
           
            return setMessage(`No more moves`)
            // console.log('No more moves');
        }   

        const boardCopy = [...board];
        boardCopy[position] = isPlayer;
        setBoard(boardCopy); // updating board for current player  


        if (isWon(boardCopy)) {
    
            console.log('Winner')
            setMessage(`WON: ${isPlayer}`)
            setIsPlayer("");
            refresh();
            return  alert(`Game Finished! Congratulations Player ${isPlayer} You have Won !!`);
           

        }


        if (boardCopy.indexOf("") === -1) {
           
            // console.log('Draw')
            setMessage("DRAW")
            setIsPlayer("");
        } else {
            let nextPlayer = (isPlayer === "X") ? "O" : "X"
            console.log('Turn')
            setIsPlayer(nextPlayer); 
            setMessage(`TURN: ${nextPlayer}`)
        }
    }

    return (
        <div>
            <h1>Tic Tac Toe </h1>
            <Message value={message} />
            <Board onClick={handleInput} value={board} />
            <Refresh onClick={refresh} value={'Reset'} />
        </div>
    )

}

export default Game