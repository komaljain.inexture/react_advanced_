import React, { useEffect, useRef } from 'react'
import './App.css';
import Input from './components/input';

function App() {
  const firstNameRef = useRef(null);
  const lastNameRef = useRef(null);
  const submitRef = useRef(null);

  useEffect(() => {
    firstNameRef.current.focus();

  }, []);
  function firstKeyDown(e) {
    if (e.key === "Enter") {
      lastNameRef.current.focus();
    }
  }
  function lastKeyDown(e) {
    if (e.key === "Enter") {
      submitRef.current.focus();
    }
  }
  function submitKeyDown() {
    alert('Form Submitted')
  }

return (
  <div className="App">
    <header className="App-Header">

      <Input
        type="text"
        onKeyDown={firstKeyDown}
        ref={firstNameRef}
        placeholder="Enter first name"
      />
      <Input
        type="text"
        onKeyDown={lastKeyDown}
        ref={lastNameRef}
        placeholder="Enter last name"
      />
      <button
        onKeyDown={submitKeyDown}
        ref={submitRef}>
        Submit
      </button>

    </header>
  </div>
)
}
export default App;
