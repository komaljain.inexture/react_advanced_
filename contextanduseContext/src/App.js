import React from 'react';
import FunctionContextComponent from './component/FunctionContextComponent';
// import ClassContextComponent from './component/ClassContextComponent'
import { ThemeProvider } from './ThemeContext';

export const ThemeContext = React.createContext();

export default function App() {
 
return(
  <>
    <ThemeProvider>
    
      <FunctionContextComponent />
      {/* <ClassContextComponent /> */}
    </ThemeProvider>
   
  </>
)
}
