import React from 'react';
import { useTheme,useThemeUpdate } from '../ThemeContext';


export default function FunctionContextComponent(){
    const darkTheme = useTheme()
    const toggleTheme = useThemeUpdate()

    const themeStyles = {
        backgroundColor : darkTheme ? '#000' : '#CCC',
        color: darkTheme ? '#CCC' : '#000', 
        padding: '3rem',
        margin : '2rem'
    }
        return(
            <>
            <button onClick={toggleTheme}>Toggle Theme</button>
            <div style={themeStyles}>Function Theme</div>
            </>
        )
}