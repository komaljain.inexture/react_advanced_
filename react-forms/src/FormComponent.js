import React, { useState } from 'react';

const Form = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const[allEntry, setAllEntry]= useState([])

    const submitForm = (e) => {
        e.preventDefault()
        const newEntry = {email:email, password:password};

        setAllEntry([...allEntry, newEntry]);
        console.log(allEntry)
    }

    return (
        <div className="box">
            <form action="" onSubmit={submitForm}>
                <div className="input_field">
                    <label htmlFor="Email">Email</label><br />
                    <input type="text" name="email" id="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </div><br />
                <div className="input_field">
                    <label htmlFor="Password">Password</label><br />
                    <input type="password" name="password" id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div><br />
                <button type="submit">Login</button>
            </form>
            <div >
                {
                    allEntry.map((curElement)=>{
                        return(
                            <div className="data">
                                <p>{curElement.email}</p>
                               <p className="data_password"> {curElement.password}</p>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}
export default Form