import React, { useState } from 'react'
import Modal from './Modal'

const BOX={
  width: "300px",
  height: "200px",
  margin: "50px 10px 10px 500px",
  backgroundColor: 'off-white',

}
const BUTTON_WRAPPER_STYLES = {
  position: 'relative',
  zIndex: 1,
  align: 'center'
}

const OTHER_CONTENT_STYLES = {
  position: 'relative',
  zIndex: 2,
  backgroundColor: 'red',
  padding: '10px',
  width :"50%"
}

export default function App() {
  const [isOpen, setIsOpen] = useState(false)
  return (
    <div style={BOX}>
      <div style={BUTTON_WRAPPER_STYLES} onClick={() => console.log('clicked')}>
        <button onClick={() => setIsOpen(true)}>Open Modal</button>

        <Modal open={isOpen} onClose={() => setIsOpen(false)}>
          Fancy Modal
        </Modal>
      </div>
      <br/>
      <div style={OTHER_CONTENT_STYLES}>Other Content</div>
    </div>
  )
}